This program automates a game in nfc.wonaruto.com (NFC)

The basic idea is to, on the bot initialization search for a disponible "round", enter it then fight.
For this, we think of modeling it into multiples little modules that will work together to create the final program, with this using oriented-object concepts for the implementation.

The global implementation steps could be written as follows:
- Upon launched, a bootstrap module will handle of the sign in of the user, searching if the next fight is available then register the account in the fight (will see about using fighting items later). The module will block until the next fight is accessible (with a report each minute how many times until it is available.)
- Once the account is registered, the bootstrap module is done working. From there, a core module will do the job. a method in this module will wait for the fight to start. Then, for each step of the fight, another function will be responsible to refresh the page polling for any ennemie's action before deciding what to do.If any move hasn't been made in lets say 50 seconds, a "best_move" variable will be assigned the best action to take at the time due to lack of information. For each step of the fight, a make_action() method will be defined to do all the actions.

NB: For the core module to be extensible, we wont implement any big chunk of code inside it. Instead we will create multiple utility classes which objects will be used by the core module.

Must-created classes are:
- Round: will contain general infos about the "round":
    * number of survivors
    * remaining survivors
    * id of the "round"
    * is_dead: contains a boolean to check wether the account can still perform or not

- Salle: will contain general infos about the room we are fighting in:
    * Type of room ("desert", "foret", "marrais", "plaine", "rochers") (will be linked with jutsus later on)
    * number of ennemies

- Adversaire: contains informations about each ennemy
    * identification_lelvel: level of identification (1-6)
    * state: ["caché", "sonné", "blessé", "paralysé", "endormi", "saoul", ...]
    * max_hp: max life of ennemy
    * current_hp: his current hp
    * strenght: Actual strenght of the ennemy
    * num_of_items: Number of items the ennemy has with him
    * get_latest_actions() : Parse latest actions the ennemy made
    * get_items() : Parse the items the ennemy has with him
    * items : [Item]

- Joueur: contains all informations about our account in the game
    * max_hp: my max life
    * current_hp: current life
    * strength: actual strenght
    * aggresivity: level of aggresivity ["nulle", "faible", "moyen", "forte"]
    * get_items() : Parse the items i have / return Item.quantity + " " + Item.name
    This class will also contains special methods to make the actions
    * make_action(...params)
    * items: [Item]
    * ennemies: [Adversaire]
- Item: contains an item which can be used throughout the game
    * name : name
    * quantity : By default will be 1, incremented after parsing
    * category : ["identification", "soin", "offensif", "fiole"]
- Etat_precedent: contains all informations about the previous state of the game, useful to help make a "better" decesion for the current sate. Basically, this class should contain a copy of the above listed classes and must be refreshed at the end of each iteration.

That's it for the basic utility classes.

Now we need a proper handling of the events: the game is highly dependant of time and so we need a flexible way of reacting to events. Feel free to add more ideas to this list.
